---
title: "Judge Disallows Data Breach Class Action Against Bankrupt California Hospital Chain"
date: 2019-06-04
author: "S.M. Oliva"
tags: [Verity Health, California, bankruptcy]

---

In April 2016, Verity Health System of California, Inc., sustained a data breach after a human resources employee provided the six-hospital network's W-2 tax information to an unknown person posing as a Verity executive. Verity did not discover the breach until about a month later, at which time it notified its employees. 

Dissatisfied with Verity's response, two Verity employees filed a class action complaint against the nonprofit health system in Los Angeles Superior Court in May 2017. A third employee filed a similar class action in San Mateo Superior Court a week later. In June 2018, both proposed class actions were consolidated before the Los Angeles court.

## Chapter 11 Filing Stayed State Data Breach Lawsuits

However, [Verity filed for Chapter 11 bankruptcy protection in August 2018](https://www.latimes.com/business/la-fi-verity-health-bankruptcy-20180831-story.html), before the consolidated state lawsuits could proceed. The three state court plaintiffs subsequently filed a motion with the bankruptcy court seeking "authorization to file a class prepetition unsecured proof of claim on behalf of similarly situated creditors." 

In plain English, the employees presented their lawsuit--which they filed before Verity sought bankruptcy protection--to the bankruptcy court and asked it be treated as a creditor claim against Verity's assets. This is necessary because when a company files for bankruptcy, it automatically stays (suspends) any pending litigation against it. The creditor--the person who sued the bankrupt company--must then initiate a separate adversary proceeding before the bankruptcy court.

But the bankruptcy court has the discretion to disallow the creditor's "proof of claim" under certain rules. In this case, the bankruptcy judge had to decide whether it was appropriate to permit the creditors to present their claim as a class action, i.e., on behalf of all current and former Verity employees affected by the 2016 data breach. 

## Judge: Class Action Would "Adversely Affect" Bankruptcy Proceeding

On May 24, 2019, U.S. Bankruptcy Judge Ernest M. Robles [denied the employees' authorization to file their proof of claim](https://scholar.google.com/scholar_case?case=4801189795525234567). In assessing the employees' motion, Judge Robles started by applying a three-part test used by other bankruptcy courts in deciding whether to permit a class action claim under these circumstances. These three tests are:

1. Was the class action certified before the company filed for bankruptcy?
2. Did all of the members of the proposed class receive notice before the "bar date," i.e., the deadline for filing creditor claims with the bankruptcy court?
3. Would certifying a creditor class action "adversely affect the administration of the estate," i.e., the bankruptcy company's assets?

Judge Robles said all three factors weighed against allowing the employees' class action to proceed as a creditor claim:

1. The original state lawsuit was not certified as a class action prior to Verity filing for bankruptcy.
2. Verity provided notice of the "bar date" to employees working for the company as of the date of the bankruptcy petition; Judge Robles acknowledged, however, that Verity did not notify former employees who were affected by the data breach, so he extended the bar date for this group to September 30, 2019.
3. Allowing all current and former Verity employees to proceed as a class would "adversely affect" the administration of the bankruptcy estate; individual employees can still present their individual claims to the bankruptcy court.

Regarding this third point, Judge Robles explained that "only a very small percentage of the putuative class has suffered any damages on account of the data breach." Indeed, only about 30 current and former Verity employees--about 0.4 percent of the total proposed class--reported any problems at all attributable to the breach. Most of these problems involved someone filing fraudulent tax returns on the employee's behalf, and the judge noted none of these attempts were successful.

That said, there were some class members who incurred minor out-of-pocket expenses related to the data breach. For example, six Verity nurses asked for reimbursement of their "time and mileage" to meet with the IRS to straighten out the fraudulent tax returns. But the judge said those individuals could still file individual proofs of claim.

## More Legal Troubles Ahead for Verity

Verity's six hospitals are currently being sold off as part of the Chapter 11 bankruptcy process. But going forward, Verity's successor ownership may be faced with additional data breach lawsuits. This year, Verity reported two new data breaches. In January, the nonprofit disclosed it fell prey to two more phishing attacks that gave an unknown person "access to three employee web email accounts," according to [HealthITSecurity](https://healthitsecurity.com/news/verity-reports-third-data-breach-caused-by-employee-email-hack). And even before Verity could disclose that second breach, a third breach occurred when the "the Microsoft 365 web email account of one employee was compromised for several hours." This third attack resulted in the disclosure of "protected health information for about 14,894 patients."
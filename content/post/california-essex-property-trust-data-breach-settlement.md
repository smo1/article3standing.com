---
title: "Calif. Real Estate Trust Pays $350,000 to Settle Employee Data Breach Lawsuit"
date: 2019-05-29
author: "S.M. Oliva"
tags: [Essex Property Trust, California]

---

In September 2014, Essex Property Trust, Inc., disclosed there was a "cyber attack" that caused a data breach of some of its computer networks. More precisely, an Essex employee fell victim to a phishing scam and sent copies of the company's 2015 W-2 tax forms to an unknown party. These forms contained the names, social security numbers, and salary information for approximately 2,400 people who worked for Essex during the 2015 tax year.

An Essex employee subsequently filed a [class action complaint](https://www.courtlistener.com/recap/gov.uscourts.cand.297395.1.0.pdf) in California state court against the company, accusing it of negligence, breach of implied contract and invasion of privacy. The complaint asked for a court order declaring Essex's existing data security measures insufficient and awarding monetary damages to the employees affected by the W-2 breach. Essex later had the case removed to federal court, and in March 2019 the parties announced a settlement.

Under the terms of the settlement, Essex agreed to establish a "gross settlement fund" of $350,000. Individual class members--i.e., the affected employees--are expected to receive approximately $70 each from this fund. Additionally, Essex will pay for up to three years of "identify protection coverage," which will be provided by Equifax. Finally, the settlement requires Essex to make additional payments of $5,000 to the plaintiff who filed the initial complaint, $140,000 in attorney's fees to the plaintiff's attorneys, and $9,500 to cover additional courts costs and expenses.

In [approving the settlement](https://scholar.google.com/scholar_case?case=14034820540574315602), U.S. District Judge Haywood S. Gilliam, Jr., said the final amount was "reasonable in light of the strength of the underlying case." The judge said the plaintiff herself conceded there were "factual and legal hurdles were she to continue litigating," notably establishing [Article III standing](https://article3standing.com/about). Given these risks, settling now has "ensured a favorable recovery for the class." Judge Gilliam further noted that only a handful of class members--12 out of 2,406--indicated they would opt-out of the settlement.

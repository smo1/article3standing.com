---
title: "Sixth Circuit: Liquor Store Chain Does Not Have to Reimburse Payment Processor for Data Breach"
date: 2019-06-17
author: "S.M. Oliva"
tags: [Spec's, First Data, Tennessee, 6th Circuit]

---

When you purchase goods from a retailer using a credit or debit card, you make what seems to be a seamless transaction. But in reality, there is a complex network of legal agreements involved. The retailer contracts with an intermediary payment processor, who in turn contract with banks that complete the transactions. As part of these contracts, retailers are generally required to follow certain data security standards to ensure the safety of the overall payment system.

In a recent case before the U.S. Sixth Circuit Court of Appeals in Cincinnati, [*Spec's Family Partners, Limited v. First Data Merchant Services LLC*](https://scholar.google.com/scholar_case?case=14520684229072199330), the judges considered to what extent a contract between a payment processor and a retailer protected the former in the event of a data breach caused by the latter.

## Retailer Fails to Follow Data Security Standards, Leading to Breach of 550,000 Customer Records

The plaintiff in this case operates a chain of liquor stores based in Texas. Like most retailers, the plaintiff accepts payment cards. To that end, the plaintiff had a contract with the defendant, who served as an intermediary payment processor.

Over a 17-month period in 2012 and 2013, attackers managed to compromise the plaintiff's payment card system. This enabled the attackers to obtain "as many as 550,000 sets of customer bank and card records," according to [Naked Security](https://nakedsecurity.sophos.com/2014/04/02/texan-liquor-chain-specs-leaks-550k-card-details-in-17-month-breach/). The breach was initially discovered by banks and credit card companies. The Sixth Circuit explained in its opinion that a subsequent investigation determined the plaintiff "failed to comply with the Payment Card Industry Data Security Standard prior to the attacks, leaving it vulnerable to the breaches."

The breach set off a chain reaction. First, the banks that issued the credit cards had to reimburse and replace the cards of the affected cardholders. Then the bank that processed the bad charges needed to reimburse the issuing banks. The defendant then reimbursed that bank. And the defendant then tried to obtain reimbursement from the plaintiff by withholding more than $6 million in proceeds from the liquor chain's legitimate credit card sales.

This prompted the plaintiff to sue, alleging the defendant had breached the "merchant agreement" between the two parties. A federal judge in Tennessee agreed with the plaintiff and held the defendant in breach of contract. The judge awarded damages to the plaintiff plus interest.

## Data Breach Losses a Form of "Consequential" Rather Than Direct Damages

On appeal to the Sixth Circuit, the defendant argued that, contrary to the trial judge's findings, the merchant agreement required the plaintiff to indemnify the defendant from "any and all claims, demands, losses, costs, liabilities, damages, judgments, or expenses arising out of or relating to" the plaintiff's breach of the agreement, including any "operating rules or regulations" of the credit card networks. According to the plaintiff, this indemnification language meant the plaintiff clearly had to compensate the defendant for the financial losses it suffered as a result of the data breach.

But the plaintiff (and the trial court) pointed to another section in the merchant agreement that said neither party was responsible for "consequential damages" sustained by the other party as the result of a breach of contract. Consequential damages--also known as "special damages"--refer to indirect losses that fall outside the scope of the contract itself. 

Senior Circuit Judge [Deborah L. Cook](https://www.fjc.gov/history/judges/cook-deborah-l), writing for the Sixth Circuit in this case, said the money the defendant spent on reimbursing the bank qualified as special damages under Tennessee law, which governs the merchant agreement. While the data breach may have been "a foreseeable consequence" of the plaintiff's failure to comply with industry data security standards, the losses suffered by the defendant "did not necessarily follow" from this failure. Indeed, Judge Cook noted that "a non-compliant merchant might never suffer a data security breach." The plaintiff was therefore protected by the consequential damages exception to the indemnification clause in the merchant agreement.

### Data Breach Losses Not a Covered "Third-Party" Cost

The Sixth Circuit also rejected the defendant's attempt to invoke another section of the merchant agreement as grounds for seeking reimbursement for the data breach losses. The section in question states the plaintiff must pay "any and all third-party fees and charges associated with the use of [the defendant's] services." Judge Cook, siding with the trial judge again, said that in this context third-party fees refer to the "routine charges associated with card processing services rather than liability for a data breach." She added the only other federal appeals court to consider this same question--the Eighth Circuit--reached the same conclusion.


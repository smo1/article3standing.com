---
title: "Eight Circuit Dismisses Final Plaintiff from SuperValu Data Breach Lawsuit"
date: 2019-06-10
author: "S.M. Oliva"
tags: [SuperVALU, 8th Circuit]

---

## *In re: SuperValu, Inc., Customer Data Security Breach Litigation*

During June and July of 2014, unknown attackers managed to access the computer network used to process credit and debit card payments for SuperValu, Inc., a Minnesota-based grocery store chain. The attackers installed malware on the network, enabling them to access the names and payment card information of SuperValu customers. SuperValu publicly disclosed the data breach in August 2014. Approximately six weeks later, SuperValu disclosed a second data breach involving "different malicious software" installed to the same network. 

In June 2015, a group of 16 plaintiffs--all SuperValu customers who paid with cards during the period of the breach--sued the company in Minnesota federal court. The trial judge dismissed all of the plaintiffs' allegations, citing a lack of Article III standing. In August 2017, the U.S. Eighth Circuit Court of Appeals [upheld the dismissal of all but one of the plaintiffs](https://scholar.google.com/scholar_case?case=10290490769081582452) from the case. Unlike his co-plaintiffs, this final plaintiff alleged a "present injury in fact," specifically an unauthorized charge to his credit card following the data breach. This was enough, in the appeals court's judgment, to establish the lone remaining plaintiff's standing.

But after the case returned to the trial court in Minnesota, the judge again dismissed the remaining plaintiff's complaint after finding it "failed to state a claim" justifying any relief. Again, the plaintiff appealed the dismissal to the Eighth Circuit. This time, however, the appeals court agreed with the trial judge and upheld the dismissal in a [May 31, 2019 opinion](https://scholar.google.com/scholar_case?case=568077287558050779).

## Retailers Have No Duty Under Illinois Law to Safeguard Customer Data from Attackers

U.S. Circuit [Judge Jane L. Kelly](https://www.fjc.gov/history/judges/kelly-jane-louise), writing for the appeals court, explained the plaintiff's allegations were governed by Illinois law, which does not usually impose an "affirmative duty" on retailers to protect a customer from "criminal attack" unless there is a "special relationship" between the parties. Indeed, Judge Kelly said the Illinois Supreme Court has yet to decide whether a retailer is ever obligated to protect customer data "from hackers." In an April 2018 decision, [*Community Bank of Trenton v. Schnuck Markets, Inc.*](https://scholar.google.com/scholar_case?case=2757250570638923459), the U.S. Seventh Circuit Court of Appeals assumed, based on precedent from an Illinois intermediate appellate court, that state law "does not recognize a duty in tort to safeguard sensitive personal information." Judge Kelly said the Eighth Circuit would follow its sister court's position, given the lack of any Illinois Supreme Court ruling to the contrary.

This meant the plaintiff could not proceed with a claim for negligence against SuperValu. But the plaintiff raised several other claims, which the Eighth Circuit also rejected:

+ **Federal Trade Commission Act**. The plaintiff argued SuperValu had a duty to protect his information under the Federal Trade Commission Act. Judge Kelly acknowledged the FTC itself regularly uses the Act to take enforcement action "against companies that have purportedly failed to protect consumer financial data against hackers." But, she added, the FTC Act does not allow individual consumers to file a lawsuit.
+ **Illinois consumer protection law**. The plaintiff alleged he suffered a legal injury under the Illinois Consumer Fraud and Deceptive Business Practices Act (ICFA). But the Eighth Circuit said the ICFA requires proof the plaintiff suffered a measurable financial loss as a result of the data breach. Here, the plaintiff said he spent additional time monitoring his account after discovering the fraudulent charge. Judge Kelly said this was not an "out-of-pocket loss," as the plaintiff was simply guarding against the speculative threat of "future identity theft."
+ **Unfair and deceptive trade practices**. Similar to the FTC Act, Illinois has its own unfair and deceptive trade practice law. Judge Kelly said the plaintiff could not recover any monetary damages under this law, however, as it only permitted courts to grant "injunctive relief" against SuperValu's future actions.
+ **Breach of contract**. Judge Kelly said there was no contract, express or implied, between SuperValu and the plaintiff that could justify a claim for a breach of contract.
+ **Unjust enrichment**. The plaintiff argued that had SuperValu informed customers of the data breaches when they were discovered--as opposed to several weeks later--he would have "never" shopped at the store. On this basis the plaintiff brought a claim for "unjust enrichment" under Illinois law. Judge Kelly said "[c]ommon sense counsels against" such a definition of unjust enrichment. 



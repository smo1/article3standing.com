---
title: "Calif. Judge Stays FriendFinder Data Breach Lawsuit Pending Arbitration Decision"
date: 2019-05-29
author: "S.M. Oliva"
tags: [FriendFinder, California, arbitration]

---

It is common practice for commercial websites to contain a "terms of service" page that purport to outline the user's legal rights. Many of these terms pages include a binding arbitration clause. In other words, if any legal dispute arises between the user and the service provider, the matter must be resolved in binding arbitration rather than a courtroom.

## Did the Plaintiff Agree to the Online "Terms of Service"?

For example, a federal judge in San Jose, California, recently put a data breach lawsuit on hold due to the existence of just such an arbitration clause. The case, [*Gutierrez v. FriendFinder Networks, Inc.*](https://scholar.google.com/scholar_case?case=4391681932007219973), arose from a November 2016 disclosure by FriendFinder Networks that six of its databases "were compromised," exposing personal information associated with over 412 million user accounts. According to [CSO](https://www.csoonline.com/article/3139311/412-million-friendfinder-accounts-exposed-by-hackers.html), the data breach also included "source code from FriendFinder Networks' production environment."

The plaintiff in this case was a longtime user of FriendFinder's adult dating website. The plaintiff made a number of credit card purchases from FriendFinder, and he alleged his personal information was disclosed in the data breach. He subsequently sued FriedFinder on a number of grounds under California law, including negligence, violation of California's Customer Records Act, and violation of California's Online Privacy Act. The plaintiff also moved to certify a class action on behalf of himself and all others affected by the breach.

FriendFinder moved to stay (delay) the lawsuit and compel arbitration based on the company's terms of service, which it said was binding on the plaintiff and all of its other users. The plaintiff maintained he never agreed to an arbitration clause, and even if he did, the provisions cited by FriendFinder were "unconscionable" and therefore legally unenforceable.

In response, FriendFinder pointed to at least three ways in which the plaintiff agreed to the terms of service, and therefore the arbitration agreement: First, when he joined FriendFinder in 2003, the registration process required him to agree to the terms of service, which included the arbitration clause at issue. In addition, the plaintiff "confirmed his agreement" every time he made a credit card purchase. Finally, after the plaintiff was denied access to one of FriendFinder's chat rooms, he was once again required to agree to the terms of service before his privileges were restored.

U.S. District Judge Beth Labson Freeman said the plaintiff did not dispute the third point--i.e., he agreed to the terms of service to get back into the chat room--and that was enough to prove he agreed to the terms of service.

## FriendFinder Allowed to User Customer Service Calls as Evidence

Indeed, one of the key pieces of evidence presented by FriendFinder was a recording of a call between the plaintiff and one of FriendFinder's customer service representatives. Although Judge Freeman said the call did not show the plaintiff "expressly" agreeing to the terms of service, the customer service representative did mention the terms, specifically in the context of explaining why he was banned from the chat room. This, the judge said, was enough to put the plaintiff "on notice" of his need to comply with the terms of service. And since he continued to use the FriendFinder website, that was sufficient to indicate "acceptance of the terms."

Aside from the merits of the evidence itself, Judge Freeman also rejected the plaintiff's challenge to the admissibility of the telephone call in the first place. The plaintiff said he was told at the time his conversation would only be recorded for "quality control purposes," and not as potential evidence against him in litigation. But the judge said FriendFinder did nothing wrong. To the contrary, it informed the plaintiff he was being recorded and gave his consent.

## Only the Arbitrator Can Decide If Arbitration Is "Unconscionable"

The final objection raised by the plaintiff was that even if he agreed to arbitration, the arbitration clause was still "unconscionable" under California law. Judge Freeman declined to address this issue. She determine the terms of the arbitration clause--which applies to all disputes "relating to these Terms"--delegated this question exclusively to the arbitrator. Put another way, only the arbitrator can decide the scope of the arbitrator's authority to arbitrate this case. And this delegation is not itself "unconscionable," Judge Freeman said.

Given all this, Judge Freeman granted FriendFinder's motion to compel arbitration and stay the plaintiff's lawsuit for the time being. Should the arbitrator ultimately decide all of the plaintiff's allegations are covered by the arbitration agreement, the court will dismiss the lawsuit. But if the arbitrator decides that only some claims are covered--or the arbitration clause itself is unconscionable--the plaintiff may be able to return to court later.


---
title: About This Blog
date: 2019-05-29
menu: main
---

## What Is This Blog About?

*Article 3 Standing* is an independent blog that covers data breach litigation in the United States. Most of this blog's content consists of summaries of recent federal and state court decisions in cases where one or more plaintiffs are seeking damages against a company that has previously disclosed a data breach. Some of the common legal questions addressed in these cases include:

+ Does the plaintiff have Article III standing?
+ What are the legal obligations of businesses (and government agencies) to safeguard private data?
+ Is there a binding agreement requiring arbitration of the plaintiff's claims outside of court?
+ If the parties have agreed to settle, are the terms fair to all of the parties concerned?

This blog does not advocate for any particular outcome in a given lawsuit. Its purposes are purely information and educational. Also note the author is not an attorney, and nothing contained on this blog constitutes legal advice of any kind.

## What Does "Article 3 Standing" Mean?

[Article III](https://www.law.cornell.edu/constitution/articleiii#), Section 2, of the U.S. Constitution limits the jurisdiction of federal courts to actual "cases" or "controversies." The [United States Supreme Court](https://scholar.google.com/scholar_case?case=10150124802357408838) interprets that to mean a plaintiff cannot proceed with a lawsuit in federal court without first establishing three things:

1. The plaintiff suffered "an invasion of a legally protected interest which is (a) concrete and particularized and (b) actual or imminent," rather than hypothetical.
2. The plaintiff must allege a "causal connection" between this actual injury and the defendant's conduct.
3. It is "likely," rather than merely "speculative," that a favorable court decision will actually redress the plaintiff's injury.

With respect to data breach litigation, courts throughout the U.S. are divided on whether the mere unauthorized disclosure of personal information is, by itself, enough to give an affected party Article III standing. This blog aims to explore the ongoing legal debate.